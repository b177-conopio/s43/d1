// Mock database
let posts = [];
// Movie post ID
let count = 1;

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// Prevents the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	// posts array
	showPosts(posts);
	alert('Movie Post successfully added');
})

// Show posts 

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post (editPost from line 34)
const editPost = (id) => {
	// from line 32
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// from html line 20
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

// Update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	/*
		i = 0, posts.length = 2
		posts[0].id = 1 === '#txt-edit-id' = 2
		posts[1].id = 2 === '#txt-edit-id' = 2

	*/

	// i=0 comes from index 0 of array
	for(let i = 0; i < posts.length; i++){
		// #txt-edit-id from line 19 html
		if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated.');

			break;
		}	

	}
})

// Delete movie
const deletePost = (id) => {
	for(let i = 0; posts.length > 1; i++){
		if(posts[i].id == id){
			posts.splice(i, 1)
			alert('Movie was deleted.')
			showPosts(posts)
			break;
		}
	}

}

